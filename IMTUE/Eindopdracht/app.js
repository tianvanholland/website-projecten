var DOMstrings = {
    landingPage: document.querySelector(".landing_page"),
    logo: document.querySelector(".logo"),
    cityPng: document.querySelector(".city_png"),
    telefoonPage: document.querySelector(".telefoons_page"),
    achtergrond: document.querySelector(".achtergrond"),
    breadcrumbSectie1: document.getElementById("sectie1"),
    breadcrumbSectie3: document.getElementById("sectie3"),
    breadcrumbSectie4: document.getElementById("sectie4"),
    breadcrumbSectie5: document.getElementById("sectie5"),
    pagina: document.querySelector(".de_pagina"),
    keuzeBtn: document.querySelector(".keuze-btn"),
    abonnementPage: document.querySelector(".abonnement_page"),
    progressBar: document.querySelector(".progress-bar"),
    stap1: document.getElementById("js--stap1"),
    stap2: document.getElementById("js--stap2"),
    stap3: document.getElementById("js--stap3"),
    stap4: document.getElementById("js--stap4"),
    abonnementImg: document.querySelector(".abonnement_img"),
    naamTelefoon: document.querySelector(".naam-telefoon"),
    indexTekst: document.querySelector(".index-tekst"),
    deKosten: document.querySelector(".deKosten"),
    welkeMobiel: document.getElementById("welkeMobiel"),
    prijsMobiel: document.getElementById("prijsMobiel"),
    welkPakket: document.getElementById("welkPakket"),
    prijsPakket: document.getElementById("prijsPakket"),
    prijsMaand: document.getElementById("prijsMaand"),
    prijsEenmalig: document.getElementById("prijsEenmalig"),
    volgendeKnop1: document.getElementById("volgende-knop1"),
    gegevensPage: document.querySelector(".gegevens_page"),
    gegevensForm: document.getElementById("gegevens_form"),
    nummerbehoudPage: document.querySelector(".nummerbehoud_page"),
    huidigNummer: document.getElementById("huidig-nummer"),
    nummerbehoudForm: document.getElementById("nummerbehoud_form"),
    mijnNummer: document.getElementById("mijn-nummer"),
    overzichtPage: document.querySelector(".overzicht_page"),
    overzichtContainer: document.getElementById("overzicht_container")
};

var mobielen = {
    galaxy64: {
        naam: "Samsung Galaxy S9 64 GB",
        prijs: 300,
        foto: "Afbeeldingen/samsung-galaxy-s9-black-front.png"
    },
    galaxy128: {
        naam: "Samsung Galaxy S9 128 GB",
        prijs: 380,
        foto: "Afbeeldingen/samsung-galaxy-s9-black-front.png"
    },
    galaxy256: {
        naam: "Samsung Galaxy S9 256 GB",
        prijs: 450,
        foto: "Afbeeldingen/samsung-galaxy-s9-black-front.png"
    },
    iphoneXr64: {
        naam: "iPhone Xr 64 GB",
        prijs: 600,
        foto: "Afbeeldingen/iphone-xr-64-gb-black-front.png"
    },
    iphoneXr128: {
        naam: "iPhone Xr 128 GB",
        prijs: 700,
        foto: "Afbeeldingen/iphone-xr-64-gb-black-front.png"
    },
    iphoneXr256: {
        naam: "iPhone Xr 256 GB",
        prijs: 780,
        foto: "Afbeeldingen/iphone-xr-64-gb-black-front.png"
    },
    Huawei20: {
        naam: "Huawei Mate 20",
        prijs: 400,
        foto: "Afbeeldingen/huawei-mate-20-twillight-front.png"
    },
    Huawei20Pro: {
        naam: "Huawei Mate 20 Pro",
        prijs: 520,
        foto: "Afbeeldingen/huawei-mate-20-pro-black-front.png"
    },
    HuaweiP20Lite: {
        naam: "Huawei P20 Lite",
        prijs: 200,
        foto: "Afbeeldingen/huawei-p20-lite-black-front.png"
    }
};

var stappen = [DOMstrings.stap1, DOMstrings.stap2, DOMstrings.stap3, DOMstrings.stap4];

function welkeStap(stap) {
    var i;
    for (i = 0; i < stappen.length; i++) {
        if (stappen[i].getAttribute("class") === "notActive") {
            return;
        }
        if (i === (stap - 1)) {
            stappen[i].setAttribute("class", "active");
        } else {
            stappen[i].setAttribute("class", "");
        }
    }
}

// alle ingevoerde gegevens worden gewist en alles wordt op de oorspronkelijke waarde gezet.
function allesOpNull() {
    var i, inputs;
    DOMstrings.keuzeBtn.innerHTML = "Kies je abonnement:";
    DOMstrings.welkPakket.innerHTML = "";
    DOMstrings.prijsPakket.innerHTML = "";
    DOMstrings.prijsMaand.innerHTML = "€ 0,00";
    DOMstrings.volgendeKnop1.setAttribute("onclick", "alert('Je moet eerst een abonnement kiezen voordat je naar de volgende stap kunt.')");
    for (i = 0; i < stappen.length; i++) {
        stappen[i].setAttribute("class", "notActive");
        stappen[i].style.backgroundColor = "transparent";
    }
    inputs = document.getElementsByTagName("input");
    for (i = 0; i < inputs.length; i++) {
        inputs[i].style.border = "1px solid #222";
    }
    stappen[0].setAttribute("class", "");
    DOMstrings.gegevensForm.reset();
    DOMstrings.nummerbehoudForm.reset();
}

function maakOverzicht() {
    // Wis eerst alle bestaande gegevens (mochten die er zijn, omdat je achteraf nog iets aanpaste)
    while (DOMstrings.overzichtContainer.firstChild) {
        DOMstrings.overzichtContainer.removeChild(DOMstrings.overzichtContainer.firstChild);
    }

    // maak dan een kloon van alle ingevulde gegevens om opnieuw weer te geven
    var deMobiel, alleKosten, lijn, deGegevens, nummerBehoud;
    deMobiel = DOMstrings.naamTelefoon.cloneNode(true);
    alleKosten = DOMstrings.deKosten.cloneNode(true);
    lijn = document.createElement("hr");
    deGegevens = DOMstrings.gegevensForm.cloneNode(true);
    nummerBehoud = DOMstrings.nummerbehoudForm.cloneNode(true);

    // haal de "volgende" button weg van de kloon
    deGegevens.removeChild(deGegevens.childNodes[40]);
    nummerBehoud.removeChild(nummerBehoud.childNodes[7]);

    // haal overige tekst weg
    deGegevens.removeChild(deGegevens.childNodes[37]);

    // voeg alle klonen toe aan het overzicht
    DOMstrings.overzichtContainer.appendChild(deMobiel);
    DOMstrings.overzichtContainer.appendChild(alleKosten);
    DOMstrings.overzichtContainer.appendChild(lijn);
    DOMstrings.overzichtContainer.appendChild(deGegevens);
    DOMstrings.overzichtContainer.appendChild(nummerBehoud);
}

function alertWaarschuwing(page) {
    var keuze = confirm('Weet je zeker dat je de pagina "Abonnement samenstellen" wilt verlaten?\nJe gegevens worden dan niet opgeslagen.');
    if (keuze) {
        DOMstrings.breadcrumbSectie1.setAttribute("onclick", "landingPage()");
        DOMstrings.logo.setAttribute("onclick", "landingPage()");
        DOMstrings.breadcrumbSectie3.setAttribute("onclick", "telefoonPage()");
        // wis alle gegevens
        allesOpNull();
        if (page === 0) {
            landingPage();
        } else {
            telefoonPage();
        }
    }
}

function zetWaarschuwing() {
    DOMstrings.breadcrumbSectie1.setAttribute("onclick", "alertWaarschuwing(0)");
    DOMstrings.logo.setAttribute("onclick", "alertWaarschuwing(0)");
    DOMstrings.breadcrumbSectie3.setAttribute("onclick", "alertWaarschuwing(1)");
}

function landingPage() {
    DOMstrings.landingPage.style.display = "inline";
    DOMstrings.cityPng.style.display = "inline";
    DOMstrings.telefoonPage.style.display = "none";
    DOMstrings.achtergrond.style.display = "none";
    DOMstrings.pagina.style.display = "none";
    DOMstrings.abonnementPage.style.display = "none";
    DOMstrings.gegevensPage.style.display = "none";
    DOMstrings.nummerbehoudPage.style.display = "none";
    DOMstrings.overzichtPage.style.display = "none";
}

function telefoonPage() {
    DOMstrings.landingPage.style.display = "none";
    DOMstrings.cityPng.style.display = "none";
    DOMstrings.progressBar.style.display = "none";
    DOMstrings.telefoonPage.style.display = "block";
    DOMstrings.achtergrond.style.display = "inline-block";
    DOMstrings.pagina.style.display = "block";
    DOMstrings.breadcrumbSectie3.setAttribute("class", "active");
    DOMstrings.breadcrumbSectie4.style.display = "none";
    DOMstrings.breadcrumbSectie5.style.display = "none";
    DOMstrings.abonnementPage.style.display = "none";
    DOMstrings.indexTekst.innerHTML = "Kies een telefoon";
    DOMstrings.gegevensPage.style.display = "none";
    DOMstrings.nummerbehoudPage.style.display = "none";
    DOMstrings.overzichtPage.style.display = "none";
    voorkantFoto();
}

function abonnementPage() {
    DOMstrings.abonnementPage.style.display = "block";
    DOMstrings.progressBar.style.display = "flex";
    DOMstrings.telefoonPage.style.display = "none";
    DOMstrings.breadcrumbSectie3.setAttribute("class", "kliksectie");
    DOMstrings.breadcrumbSectie4.style.display = "block";
    DOMstrings.breadcrumbSectie5.style.display = "block";
    DOMstrings.breadcrumbSectie5.setAttribute("class", "active");
    DOMstrings.indexTekst.innerHTML = "Kies een abonnement bij je telefoon";
    DOMstrings.gegevensPage.style.display = "none";
    DOMstrings.nummerbehoudPage.style.display = "none";
    DOMstrings.overzichtPage.style.display = "none";
    welkeStap(1);
    scroll(0, 0); // scrollt automatisch naar de top van de pagina
}

function gegevensPage() {
    DOMstrings.abonnementPage.style.display = "none";
    DOMstrings.indexTekst.innerHTML = "Vul je gegevens in";
    DOMstrings.nummerbehoudPage.style.display = "none";
    DOMstrings.overzichtPage.style.display = "none";
    DOMstrings.gegevensPage.style.display = "block";
    welkeStap(2);
}

function nummerbehoudPage() {
    DOMstrings.gegevensPage.style.display = "none";
    DOMstrings.abonnementPage.style.display = "none";
    DOMstrings.overzichtPage.style.display = "none";
    DOMstrings.indexTekst.innerHTML = "Nummerbehoud";
    DOMstrings.nummerbehoudPage.style.display = "block";
    welkeStap(3);
}

function overzichtPage() {
    DOMstrings.gegevensPage.style.display = "none";
    DOMstrings.abonnementPage.style.display = "none";
    DOMstrings.nummerbehoudPage.style.display = "none";
    DOMstrings.overzichtPage.style.display = "block"
    DOMstrings.indexTekst.innerHTML = "Overzicht van je bestelling";
    maakOverzicht();
    welkeStap(4);
}

var fotoIndex = [];
var fotoId = ["mobielFoto1", "mobielFoto2", "mobielFoto3", "mobielFoto4", "mobielFoto5", "mobielFoto6", "mobielFoto7", "mobielFoto8", "mobielFoto9"];
var i;

function showFoto(n, no) {
    var i, x;
    x = document.getElementsByClassName(fotoId[no]);
    if (n > x.length) {
        fotoIndex[no] = 1;
    }
    if (n < 1) {
        fotoIndex[no] = x.length;
    }
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";
    }
    x[fotoIndex[no] - 1].style.display = "block";
}

// Laat de voorkant van elke mobiel zien wanneer de telefoon page opstart
function voorkantFoto() {
    for (i = 0; i < fotoId.length; i++) {
        fotoIndex = [1, 1, 1, 1, 1, 1, 1, 1, 1];
        showFoto(1, i);
    }
}

function plusFoto(n, no) {
    showFoto(fotoIndex[no] += n, no);
}

function mobielGekozen(info) {
    DOMstrings.naamTelefoon.innerHTML = info.naam;
    DOMstrings.abonnementImg.src = info.foto;
    DOMstrings.prijsMobiel.innerHTML = "€ " + info.prijs.toString() + ",00";
    DOMstrings.welkeMobiel.innerHTML = info.naam;
    DOMstrings.prijsEenmalig.innerHTML = "€ " + (info.prijs + 15).toString() + ",00";
    abonnementPage();
}

function mobielKiezen(mobiel) {
    switch (mobiel) {
        case 1:
            mobielGekozen(mobielen.galaxy64);
            break;
        case 2:
            mobielGekozen(mobielen.galaxy128);
            break;
        case 3:
            mobielGekozen(mobielen.galaxy256);
            break;
        case 4:
            mobielGekozen(mobielen.iphoneXr64);
            break;
        case 5:
            mobielGekozen(mobielen.iphoneXr128);
            break;
        case 6:
            mobielGekozen(mobielen.iphoneXr256);
            break;
        case 7:
            mobielGekozen(mobielen.Huawei20);
            break;
        case 8:
            mobielGekozen(mobielen.Huawei20Pro);
            break;
        case 9:
            mobielGekozen(mobielen.HuaweiP20Lite);
            break;
    }
}

// abonnement menu
function keuzeMaken() {
    document.getElementById("keuzes").classList.toggle("show");
    DOMstrings.keuzeBtn.classList.toggle("focus");
}

// sluit het menu wanneer er buiten het menu geklikt wordt
window.onclick = function (event) {
    if (!event.target.matches('.keuze-btn')) {
        var keuzes = document.getElementsByClassName("keuze-opties");
        var focus = DOMstrings.keuzeBtn;
        var i;
        for (i = 0; i < keuzes.length; i++) {
            var openKeuzes = keuzes[i];
            if (openKeuzes.classList.contains('show')) {
                openKeuzes.classList.remove('show');
                focus.classList.remove('focus');
            }
        }
    }
};

function keuzeGemaakt(el) {
    DOMstrings.keuzeBtn.innerHTML = "";

    // zet de data van de keuze bij het totaal per maand bedrag
    DOMstrings.welkPakket.innerHTML = el.getAttribute("data-naam");
    DOMstrings.prijsPakket.innerHTML = el.getAttribute("data-prijs");
    DOMstrings.prijsMaand.innerHTML = el.getAttribute("data-prijs");

    // Maak een kloon van de keuze die gekozen is om bovenaan het menu te zetten als "gekozen"
    var kloon = el.cloneNode(true);
    kloon.removeAttribute("onclick");
    kloon.style.pointerEvents = "none";

    DOMstrings.keuzeBtn.appendChild(kloon);

    // De stap van een abonnement kiezen is behaald, progress-bar wordt aangepast
    DOMstrings.stap1.style.backgroundColor = "rgba(144, 255, 118, 0.8)";
    DOMstrings.stap2.setAttribute("class", "");

    // de knop "volgende stap" is nu werkend gemaakt
    DOMstrings.volgendeKnop1.setAttribute("onclick", "gegevensPage()");

    // Als je nu weer terug wilt gaan naar home of een andere telefoon wilt bekijken moet je eerst een waarschuwing krijgen:
    zetWaarschuwing();
}

/*  
    Bekijk of er iets is ingevuld in de input tag wanneer deze veranderd.
    Dit is nodig voor als je terug gaat nadat je het formuleer al hebt voltooid 
    en je besluit terug te gaan om gegevens te veranderen.
    Als je de gegevens aanpast dat er een input veld leeg is moet de pagina
    nummerbehoud niet meer toegankelijk zijn totdat je het lege veld weer ingevuld hebt.    
*/
function checkGegevens(el) {
    if (el === DOMstrings.mijnNummer) {
        console.log("nummer")
        if (el.value.length === 0) {
            el.style.border = "1.5px dotted #f00";
            DOMstrings.stap3.style.backgroundColor = "rgba(0, 0, 0, 0)";
            DOMstrings.stap4.setAttribute("class", "notActive");
            DOMstrings.stap4.style.backgroundColor = "rgba(0, 0, 0, 0)";
        } else {
            el.style.border = "1px solid #222";
        }
    } else if (el.value.length === 0) {
        el.style.border = "1.5px dotted #f00";
        DOMstrings.stap3.setAttribute("class", "notActive");
        DOMstrings.stap4.setAttribute("class", "notActive");
        DOMstrings.stap2.style.backgroundColor = "rgba(0, 0, 0, 0)";
        DOMstrings.stap4.style.backgroundColor = "rgba(0, 0, 0, 0)";
    } else {
        el.style.border = "1px solid #222";
    }
}

function gegevensIngevuld(welke) {

    if (welke === "gegevens") {
        // De stap van de gegevens invullen is behaald, progress-bar wordt aangepast
        DOMstrings.stap2.style.backgroundColor = "rgba(144, 255, 118, 0.8)";
        DOMstrings.stap3.setAttribute("class", "");

        // Ga naar de volgende stap
        nummerbehoudPage();
    } else if (welke === "nummerbehoud") {
        // De stap van de gegevens invullen is behaald, progress-bar wordt aangepast
        DOMstrings.stap3.style.backgroundColor = "rgba(144, 255, 118, 0.8)";
        DOMstrings.stap4.setAttribute("class", "");
        DOMstrings.stap4.style.backgroundColor = "rgba(144, 255, 118, 0.8)";

        // Ga naar de volgende stap
        overzichtPage();
    }
}

function nummerIngevuld() {
    if (DOMstrings.mijnNummer.value.length === 0) {
        DOMstrings.stap3.style.backgroundColor = "rgba(0, 0, 0, 0)";
        DOMstrings.stap4.setAttribute("class", "notActive");
        DOMstrings.stap4.style.backgroundColor = "rgba(0, 0, 0, 0)";
    }
}

function huidigNummer(antwoord) {
    if (antwoord) {
        DOMstrings.huidigNummer.style.display = "block";
        DOMstrings.mijnNummer.setAttribute("required", "");
    } else {
        DOMstrings.huidigNummer.style.display = "none";
        DOMstrings.mijnNummer.removeAttribute("required");
    }
}

function hetEinde() {
    alert("U Heeft een mailtje ontvangen met meer informatie over de betaling. \n Bedankt voor uw aankoop.");
    allesOpNull();
    landingPage();
}
