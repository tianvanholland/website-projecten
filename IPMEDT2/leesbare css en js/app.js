var DOMstrings = {
    titellabel: '.titel_label',
    startBtn: '.start_btn',
    voorgrond: '.voorgrond',
    achtergrond: '.achtergrond',
    tekstBlok: '.tekst_blok',
    middenTekst: '.midden_tekst',
    verderTerugContainer: '.verder_terug_container',
    verderTerugBtn: '.verder_terug_btn',
    horloge: '.horloge',
    mobiel: '.mobiel',
    google: '.google',
    bericht: '.bericht',
    donkereToon: '.donkere_toon',
    zwartScherm2: '.tweede_zwart_scherm',
    nieuws: '.nieuws',
    flipscherm: '.flip',
    containerVoegToe: '.container_voeg_toe',
    okBtn: '.OK',
    pauze: '.pauze'
};

var afb = 0;
var regel = -1;
var regelID = 0;
var keuze = 2; // 2 omdat ik twee zinnen heb toegevoegd aan het normale verhaal array en ik weet even geen snelle oplossing
var licht = false; // voor opheldering van de verder en terug knoppen
var orientatie; // voor non iOS gebruikers
var keuze_zitten = false; // welke keuze is gekozen
var keuze_boos = false; // welke keuze is gekozen
var play = true; // wordt de video afgespeeld
var afspelen = false; // achtergrond geluid
var tweedeGeluid = false; // geluid voor trieste muziek of boze muziek
var achtergrondGeluid = document.getElementById("achtergrond_geluid");
achtergrondGeluid.volume = 0;
achtergrondGeluid.loop = true;
var triestGeluid = document.getElementById("triest_geluid");
triestGeluid.volume = 0;
triestGeluid.loop = true;
var onheilspellendGeluid = document.getElementById("onheilspellend_geluid");
onheilspellendGeluid.volume = 0;
onheilspellendGeluid.loop = true;

// mobiel gebruiker? Vraag of diegene een tip wil krijgen om de website op een volledig scherm te zien.
if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    if (confirm('Wil je deze website volledig op je scherm bekijken?')) {
        document.querySelector(DOMstrings.containerVoegToe).style.display = 'inline';
    } else {
        // Doe niks
    }
}

// voor iOS gebruikers
var iOS = !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);

// De afbeeldingen worden alvast ingeladen zodat alle animaties goed werken
function preload(images) {
    if (document.images) {
        var i = 0;
        var afbeeldingArray = new Array();
        afbeeldingArray = images.split(',');
        var afbeeldingObject = new Image();
        for (i = 0; i <= afbeeldingArray.length - 1; i++) {
            document.write('<img src="' + afbeeldingArray[i] + '" />');
            afbeeldingObject.src = afbeeldingArray[i];
        }
    }
}

// Afbeeldingen die ingeladen moeten worden
preload(
    'Afbeeldingen/achtergrond_0.png,Afbeeldingen/achtergrond_1.png,Afbeeldingen/achtergrond_2.png,Afbeeldingen/achtergrond_3.png,Afbeeldingen/achtergrond_4.png,Afbeeldingen/achtergrond_5.png, Afbeeldingen/achtergrond_6.png,Afbeeldingen/achtergrond_7.png,Afbeeldingen/achtergrond_8.png,Afbeeldingen/horloge_2.png,Afbeeldingen/horloge_3.png,Afbeeldingen/horloge_4.png, Afbeeldingen/google.png,Afbeeldingen/mobielFacebook.png'
);

// Sluit de tip om hoe je volledig scherm kan kijken
function sluitTip() {
    document.querySelector(DOMstrings.containerVoegToe).style.display = 'none';
}

// Moet je het scherm horizontaal draaien?
window.addEventListener("orientationchange", function () {
    flipScherm(iOS);
});

// check orientatie in het begin (wanneer de website opstart)
flipScherm(iOS);

// Check of de animatie moet komen met de vraag om het scherm te draaien
function flipScherm(iphone) {
    if (iphone) {
        if (window.orientation === 90 || window.orientation === -90) {
            document.querySelector(DOMstrings.flipscherm).style.display = "none";
        } else {
            document.querySelector(DOMstrings.flipscherm).style.display = "inline";
            flipAnimatie();
        }
    } else {
        orientatie = screen.msOrientation || (screen.orientation || screen.mozOrientation || {}).type;
        if (orientatie === "portrait-primary" || orientatie === "portrait-secondary") {
            document.querySelector(DOMstrings.flipscherm).style.display = "inline";
            flipAnimatie();
        } else if (orientatie === undefined) {
            console.log("De oriëntatie API werkt niet");
        } else if (orientatie === "landscape-primary" || orientatie === "landscape-secondary") {
            document.querySelector(DOMstrings.flipscherm).style.display = "none";
        }
    }
}

function flipAnimatie() {
    document.getElementById("scherm1").style.opacity = 1;
    setTimeout(function () {
        document.getElementById("pijl").style.opacity = 1;
        document.getElementById("scherm1").style.opacity = 0.35;
    }, 1000);
    setTimeout(function () {
        document.getElementById("pijl").style.opacity = 0.35;
        document.getElementById("scherm2").style.opacity = 1;
    }, 1400);
    setTimeout(function () {
        document.getElementById("scherm1").style.opacity = 1;
        document.getElementById("scherm2").style.opacity = 0.35;
    }, 2000);
    if (orientatie === "portrait-primary" || orientatie === "portrait-secondary" || window.orientation === 0 || window.orientation === 180) {
        setTimeout(function () {
            flipAnimatie();
        }, 2000);
    }
}

//Start knop
document.getElementById("start_btn").addEventListener("click", start);

//verder knop
document.getElementById("verder_btn").addEventListener("click", volgendeTekst);

//terug knop
document.getElementById("terug_btn").addEventListener("click", vorigeTekst);

//keuze nieuws lezen
document.getElementById("keuze_nieuws").addEventListener("click", keuzeNieuws);

//keuze weerbericht lezen
document.getElementById("keuze_weerbericht").addEventListener("click", keuzeWeer);

//keuze ga zitten
document.getElementById("keuze_zitten").addEventListener("click", keuzeZitten);

//keuze word boos
document.getElementById("keuze_boos").addEventListener("click", keuzeBoos);

//OK knop
document.querySelector(DOMstrings.okBtn).addEventListener("click", sluitTip);

//Video is afgelopen ga verder met animatie
document.querySelector(DOMstrings.nieuws).addEventListener("ended", tweedeZwartSchermVervolg);

// Hier staat het hele verhaal
var verhaal = [
    "Hij zou er bijna moeten zijn.",
    "Ik kan niet wachten tot ik mijn broer weer zie!",
    "Al twee jaar zit hij in Kuala Lumpur voor zijn werk. Gelukkig is hij eindelijk klaar met het project en komt hij weer naar huis.",
    "We zijn nog nooit zolang van elkaar gescheiden geweest. Dus moeten we het zo snel mogelijk vieren dat hij er weer is!",
    "Waarschijnlijk wilt hij eerst wat slaap inhalen, die rust krijg je namelijk echt niet op een vliegtuig, zeker hij niet.",
    "Maar als hij dan klaar is met zijn middagdutje gaan we meteen naar het feest.",
    "Onze vrienden en ik hebben samen de favoriete bar van mijn broer voor één dag afgehuurd, zodat we zijn terugkomst kunnen vieren.",
    "We kunnen darten en biljarten, maar vooral zodat we kunnen drinken zoals we altijd doen. Er is namelijk niks beter dan te drinken met je vrienden.",
    "Ik zie dat de zon begint op te komen en ik kijk naar mijn horloge.",
    "Mmm, 6:36... die vliegtuigen zijn ook nooit op tijd.",
    "\"Ghaahhw\" Ik laat een diepe geeuw mijn mond ontsnappen. Ik ben eigenlijk veel vermoeider dan ik dacht.",
    "Ik heb ook geen oog dicht gedaan vannacht, ik kon gewoon niet wachten om mijn broer op te halen van het vliegveld.",
    "Opeens voel ik mijn ogen dicht vallen.",
    "\"Huh\" Ik doe mijn ogen weer open en kijk snel om mij heen.",
    "...Oh ik was in slaap gevallen, hoelang sliep ik?",
    "...6:59...",
    "Ik kijk naar de gate, maar ik zie er nog niemand uit lopen.",
    "Ik ga maar even kijken of er iets bij de aankomst borden staat.",
    "Oh, zijn vlucht is vertraagd. Lekker dan, alsof ik nog niet lang genoeg op hem heb gewacht.",
    "Dan kan ik beter maar weer gaan zitten.",
    "Laat ik maar iets gaan doen, voordat ik weer in slaap val.",
    "Ik pakte een krant op die naast mij op de stoel lag.", //1
    "Wat zal ik lezen?", //2
    "Het is al 7:24...",
    "Ik voelde een lichte knoop in mijn maag ontstaan terwijl ik het zweet op mijn voorhoofd voelde opkomen.",
    "Relax, alles is goed.",
    "Er vliegen zoveel vliegtuigen rond, hoe groot is nou de kans dat er precies bij zijn vlucht er iets mis gaat?!",
    "Bij die laatste gedachte voelde ik mij onzeker worden.",
    "Weer ging er veel tijd voorbij.",
    "Het is al 7:47.",
    "Ik begon mij nu echt ongemakkelijk te voelen.",
    "De aankomst borden zeiden nog steeds hetzelfde: \"Flight MH370 DELAYED\".",
    "Om mij heen zag ik meer mensen die ook op die vlucht zaten te wachten. Zij hadden natuurlijk ook familie die weer thuis zou komen.",
    "Misschien weten ze meer bij de hulp-balie.",
    "Ik liep naar de hulp-balie waar ik meteen geholpen kon worden.",
    "\"Hallo, kunt u mij misschien meer vertellen over vlucht MH370?\"",
    "\"De vlucht is vertraagd meneer.\"",
    "\"Maar weet u ook waarom en voor hoelang nog?\"",
    "\"Het spijt mij meneer, maar ik kan u niet meer vertellen, dit is alles wat ik weet.\"",
    "\"Misschien kunt u informatie krijgen van Malaysia Airlines zelf op het internet.\"",
    "\"Oké, dat zal ik doen, bedankt.\"",
    "Ik pakte mijn mobiel en keek online. Malaysia Airlines heeft zojuist een bericht geplaatst op Facebook.",
    "Ik open Facebook.",
    "Wat...",
    "...Nee...",
    "...Dat kan niet...",
    "...Hoe reageer ik hier op?",
    "Ik toets het nummer in dat was gegeven in het Facebook bericht.",
    "\"Momenteel zijn al onze lijnen in gesprek, probeer het later nog eens.\"",
    "Natuurlijk, iedereen die meer wilt weten belt dat nummer nu...",
    "Dan moet ik maar wachten...",
    "Dus wachtte ik.",
    "...Ik wachtte...",
    "...En ik wachtte...",
    "De volgende dag werd ik wakker met vreselijke rugpijn van het slapen op een stoel.",
    "Mijn ogen waren vermoeid en uitgedroogd van al het huilen, ik stonk vreselijk en ik had onwijs veel honger.",
    "Ik had gister de hele dag gewacht op meer informatie over mijn broers vlucht, maar er kwam maar niks.",
    "Het nummer kon ik uiteindelijk bereiken, maar zij vertelde mij alleen maar dingen die ik al wist.",
    "Het had geen nut meer om op het vliegveld te wachten, maar ik kon maar niet weg gaan.",
    "Mijn lichaam wilde geen spier verzetten.",
    "Ook ik zelf wilde het niet geloven en hoopte dat het een nachtmerrie was en dat mijn broer elk moment door de gate kon komen lopen.",
    "Maar hij kwam maar niet...",
    "Hoelang ik ook wachtte...",
    "Met die gedachte bleef ik nog twee dagen leven op het vliegveld.",
    "Tot op de derde dag mijn vrouw mij kwam ophalen.",
    "Ik had geen energie meer om mij te verzetten, dus accepteerde ik het feit maar en ging ik met haar mee terug naar huis.",
    "...",
    "...Niemand weet nog steeds niet wat er precies gebeurd is met mijn broers vlucht...",
    "...",
    "Waar ben je?"
];

var verheldering = function () {
    document.querySelector(DOMstrings.voorgrond).addEventListener("click", knoppenLichtenOp);
};

function knoppenLichtenOp() {
    if (licht) {
        document.getElementById("verder_btn").style.backgroundColor = "rgba(255, 255, 255, 0.2)";
        document.getElementById("terug_btn").style.backgroundColor = "rgba(255, 255, 255, 0.2)";
        document.getElementById("verder_btn").style.color = "rgba(0, 0, 0, 0.5)";
        document.getElementById("terug_btn").style.color = "rgba(0, 0, 0, 0.5)";
        licht = false;
    } else {
        document.getElementById("terug_btn").style.backgroundColor = "rgba(255, 255, 255, 0.75)";
        document.getElementById("verder_btn").style.backgroundColor = "rgba(255, 255, 255, 0.75)";
        document.getElementById("verder_btn").style.color = "rgba(0, 0, 0, 0.75)";
        document.getElementById("terug_btn").style.color = "rgba(0, 0, 0, 0.75)";
        licht = true;
    }
}

var volgendeAchtergrond = function () {

    afb++;
    // 1. verander de achtergrond
    document.getElementById("achtergrond_" + (afb - 1)).style.backgroundImage = 'url(Afbeeldingen/achtergrond_' + afb + '.png)';

    // 2. verander het ID van de achtergrond
    document.getElementById("achtergrond_" + (afb - 1)).id = "achtergrond_" + afb;


};

var tekstBlok = function (regelnummer) {

    // 1. houdt bij waar het verhaal is, welke regel
    regel += regelnummer;

    // 2. voert functie specialeRegel uit om te kijken of er iets veranderd moet worden aan de display.
    specialeRegel(regel, keuze);

    // 3. return de regel (waar het verhaal is)
    return verhaal[regel];
};

var specialeRegel = function (Specregel, keuzeVariabel) {
    /* De parameter "keuzeVariabel" is nodig voor de keuze die de gebruiker maakt.
    Omdat de drie verschillende keuzes verschillende aantal regels hebben zorgt deze parameter
    ervoor dat de IF statements op de goede regel in werking gaan */

    // Er wordt gekeken of er iets speciaals is aan de regel die getoond gaat worden:
    // * De "terug" knop moet verdwijnen
    if (Specregel === 0 || Specregel === 13 || Specregel === 18 || Specregel === 21 || Specregel === 23 || Specregel === (32 + keuzeVariabel) && keuzeVariabel < 20 || verhaal[Specregel] === "Het is al 7:24..." || Specregel === (33 + keuzeVariabel) && keuzeVariabel < 20 || Specregel === (40 + keuzeVariabel) && keuzeVariabel < 20 || Specregel === (44 + keuzeVariabel) && keuzeVariabel < 20 || verhaal[Specregel] === "Ik toets het nummer in dat was gegeven in het Facebook bericht." || verhaal[Specregel] === "Ik kan het niet geloven..." || verhaal[Specregel] === "Ik draai mij onmiddelijk om naar de hulp-balie." || verhaal[Specregel] === "Ik ga weer zitten en iedereen om mij heen gaat weer verder met wat zij aan het doen waren." || verhaal[Specregel] === 'MISSCHIEN LEEFT HIJ NOG!' || verhaal[Specregel] === "De volgende dag werd ik wakker met vreselijke rugpijn van het slapen op een stoel." || verhaal[Specregel] === "Met die gedachte bleef ik nog twee dagen leven op het vliegveld.") {
        document.getElementById("terug_btn").style.display = 'none';
    }

    // * De "terug" knop moet zichtbaar gemaakt worden
    if (Specregel === 1 || Specregel === 14 || Specregel === 24 || Specregel === (34 + keuzeVariabel) && keuzeVariabel < 20 || verhaal[Specregel] === "Ik voelde een lichte knoop in mijn maag ontstaan terwijl ik het zweet op mijn voorhoofd voelde opkomen." || Specregel === (41 + keuzeVariabel) && keuzeVariabel < 20 || verhaal[Specregel] === "\"Momenteel zijn al onze lijnen in gesprek, probeer het later nog eens.\"" || verhaal[Specregel] === "\"WAT BETEKEND DIT?!\"" || verhaal[Specregel] === "Mijn broer, uit het niets, weg..." || verhaal[Specregel] === "Wacht eens er stond in nummer in dat bericht voor meer informatie." || verhaal[Specregel] === "Er stond een nummer om te bellen in dat bericht voor meer informatie." || verhaal[Specregel] === "Mijn ogen waren vermoeid en uitgedroogd van al het huilen, ik stonk vreselijk en ik had onwijs veel honger.") {
        document.getElementById("terug_btn").style.display = 'inline';
    }

    // * Nieuwe achtergrond nodig
    if (Specregel === 18 || Specregel === 20 || Specregel === (32 + keuzeVariabel) && keuzeVariabel < 20) {
        volgendeAchtergrond();
    }

    // * Horloge laten zien
    if (Specregel === 9 || Specregel === 15 || Specregel === (21 + keuzeVariabel) && keuzeVariabel < 20 || Specregel === (27 + keuzeVariabel) && keuzeVariabel < 20) {
        document.querySelector(DOMstrings.horloge).style.display = 'inline';
    }

    // * Horloge laten verdwijnen
    if (Specregel === 10 || Specregel === 8 || Specregel === 14 || Specregel === 16 || Specregel === (20 + keuzeVariabel) && keuzeVariabel < 20 || Specregel === (22 + keuzeVariabel) && keuzeVariabel < 20 || Specregel === (26 + keuzeVariabel) && keuzeVariabel < 20 || Specregel === (28 + keuzeVariabel) && keuzeVariabel < 20) {
        document.querySelector(DOMstrings.horloge).style.display = 'none';
    }

    // * Horloge tijd aanpassen
    if (Specregel === 15) {
        document.querySelector(DOMstrings.horloge).style.backgroundImage = "url(Afbeeldingen/horloge_2.png)";
    } else if (Specregel === (21 + keuzeVariabel) && keuzeVariabel < 20) {
        document.querySelector(DOMstrings.horloge).style.backgroundImage = "url(Afbeeldingen/horloge_3.png)";
    } else if (Specregel === (27 + keuzeVariabel) && keuzeVariabel < 20) {
        document.querySelector(DOMstrings.horloge).style.backgroundImage = "url(Afbeeldingen/horloge_4.png)";
    }

    // * Ogen dicht en open
    if (Specregel === 12) {
        ogenOpenDicht();
    }

    // * Mobiel laten zien
    if (Specregel === (39 + keuzeVariabel) && keuzeVariabel < 20) {
        document.querySelector(DOMstrings.mobiel).style.display = 'inline';
    }

    // * Mobiel laten verdwijnen
    if (Specregel === (38 + keuzeVariabel) && keuzeVariabel < 20 || Specregel === (44 + keuzeVariabel) && keuzeVariabel < 20) {
        document.querySelector(DOMstrings.mobiel).style.display = 'none';
        document.querySelector(DOMstrings.bericht).style.display = 'none';
    }

    // * Bericht vergroten
    if (Specregel === (40 + keuzeVariabel) && keuzeVariabel < 20) {
        document.querySelector(DOMstrings.google).style.display = 'none';
        document.querySelector(DOMstrings.mobiel).style.backgroundImage = "url(Afbeeldingen/mobielFacebook.png)";
        document.querySelector(DOMstrings.bericht).style.display = 'inline';
        document.querySelector(DOMstrings.verderTerugContainer).style.display = 'none';
        achtergrondGeluid.volume = 0.1;
        setTimeout(bekijkBericht, 2000);
    }

    // * Bericht verkleinen
    if (Specregel === (41 + keuzeVariabel) && keuzeVariabel < 20) {
        cancelBericht();
    }

    // * Keuze menu verschijnt en "verder" en "terug" knoppen verdwijnen
    if (Specregel === (44 + keuzeVariabel) && keuzeVariabel < 20) {
        document.getElementById("emotie").style.display = 'inline';
        document.querySelector(DOMstrings.verderTerugContainer).style.display = 'none';
    }

    // * Keuze menu verdwijnt en "verder" en "terug" knoppen verschijnen
    if (verhaal[Specregel] === "Ik kan het niet geloven..." || verhaal[Specregel] === "Ik draai mij onmiddelijk om naar de hulp-balie.") {
        document.getElementById("emotie").style.display = 'none';
        document.querySelector(DOMstrings.verderTerugContainer).style.display = 'inline';
        achtergrondGeluid.volume = 0.08;
    }

    // /* Keuze menu voor de krant verschijnt en "verder" en "terug" knoppen verdwijnen
    if (Specregel === 22) {
        document.getElementById("krant").style.display = 'inline';
        document.querySelector(DOMstrings.verderTerugContainer).style.display = 'none';
    }

    // * Keuze menu voor de krant verdwijnt en "verder" en "terug" knoppen verschijnen
    if (Specregel === 23) {
        document.getElementById("keuze_nieuws").style.display = 'none';
        document.getElementById("keuze_weerbericht").style.display = 'none';
        document.querySelector(DOMstrings.verderTerugContainer).style.display = 'inline';
    }

    // * keuze nieuws, krant verwijderen.
    if (Specregel === 37 && keuzeVariabel === 17) {
        document.getElementById("nieuws_bericht").style.display = 'none';
    }

    // * keuze weerbericht, krant verwijderen.
    if (Specregel === 30 && keuzeVariabel === 9) {
        document.getElementById("weerbericht").style.display = 'none';
    }

    // * Keuze "Ga zitten"
    if (keuze_zitten === true) {
        // achtergrond donkerder maken
        document.querySelector(DOMstrings.donkereToon).style.backgroundColor = "rgba(0, 0, 0, 0.6)";
        // achtergrond normaal maken
        if (verhaal[Specregel] === 'MISSCHIEN LEEFT HIJ NOG!') {
            document.querySelector(DOMstrings.donkereToon).style.display = 'none';
            speelTriestofBoosGeluid(triestGeluid);
        }
        if (Specregel === (43 + keuzeVariabel)) {
            // functie op de muziek te stoppen doormiddel van een event listener (voor Apple gebruikers)
            stopTriestBoosGeluid();
        }
    }

    // * Keuze "Word boos"
    if (keuze_boos === true) {
        if (verhaal[Specregel] === "\"Sorry... het was niet mijn bedoeling om al mijn woede op u te uiten.\"") {
            // zet muziek uit
            speelTriestofBoosGeluid(onheilspellendGeluid);
        }
        if (verhaal[Specregel] === "Ik ga weer zitten en iedereen om mij heen gaat weer verder met wat zij aan het doen waren.") {
            // verander achtergrond
            document.getElementById("achtergrond_" + afb).style.backgroundImage = 'url(Afbeeldingen/achtergrond_' + 1 + '.png)';
        }
        if (Specregel === (43 + keuzeVariabel)) {
            // functie op de muziek te stoppen doormiddel van een event listener (voor Apple gebruikers)
            stopTriestBoosGeluid();
        }
    }

    // * Dag naar Nacht overgang
    if (Specregel === (49 + keuzeVariabel)) {
        dagNachtOvergang();
        speelAchtergrondGeluid();
    }

    // * Tweede zwarte scherm
    if (Specregel === (61 + keuzeVariabel)) {
        document.querySelector(DOMstrings.voorgrond).style.backgroundColor = "rgba(0, 0, 0, 1)";
        document.querySelector(DOMstrings.zwartScherm2).style.display = 'inline';
        speelAchtergrondGeluid();
    }

    // * het achtergrond geluid stoppen (dit moet voor Apple gebruikers via een event listener...)
    if (Specregel === (63 + keuzeVariabel)) {
        document.getElementById("verder_btn").addEventListener("click", function () {
            achtergrondGeluid.pause();
        });
    }

    // * video begint
    if (Specregel === (64 + keuzeVariabel)) {
        tweedeZwartScherm();

        //Video play en pause knop, ook als je op het pauze icoontje klikt
        document.querySelector(DOMstrings.nieuws).addEventListener("click", playPause);
        document.querySelector(DOMstrings.pauze).addEventListener("click", playPause);
    }

    // * Einde
    if (Specregel === (68 + keuzeVariabel)) {
        document.getElementById("zwart_scherm").style.transition = "all 6s";
        document.getElementById("zwart_scherm").style.WebkitTransition = "all 6s";
        document.querySelector(DOMstrings.voorgrond).style.backgroundColor = "rgba(0, 0, 0, 1)";
        document.querySelector(DOMstrings.verderTerugContainer).style.display = 'none';
        document.querySelector(DOMstrings.tekstBlok).style.display = 'none';
        setTimeout(function () {
            location.reload();
        }, 7000);
    }
};

function keuzeZitten() {

    // de keuze "Ga zitten" wordt toegevoegd aan het verhaal array
    verhaal.splice((45 + keuze), 0,
        "Ik kan het niet geloven...",
        "Mijn broer, uit het niets, weg...",
        "Mijn broer, de belangrijkste familie en vriend in mijn leven, weg...",
        "Mijn broer, van jongs af aan doen wij alles samen.",
        "Hij haalde mij altijd uit de problemen. Ik hielp hem studeren.",
        "Hij zorgde dat ik een goede en leuke baan kreeg. Ik zorgde ervoor dat hij de vrouw van zijn leven ontmoette.",
        "Wij hadden nooit ruzie en wij weten alles van elkaar. Maar nu...",
        "Nu is hij weg.",
        "Zomaar zonder waarschuwing WEG!",
        "HIJ HAD NOG EEN HEEL LEVEN VOOR HEM!",
        "Zijn vrouw en zijn zoontje...",
        "Ik was er niet eens bij, om vaarwel tegen hem te zeggen...",
        "Wacht... misschien is hij helemaal niet weg...",
        "Ze zijn alleen het vliegtuig kwijt...",
        "Misschien moest het vliegtuig een noodlanding maken...",
        "MISSCHIEN LEEFT HIJ NOG!",
        "Er stond een nummer om te bellen in dat bericht voor meer informatie."
    );
    keuze += 17;


    // 1. verander achtergrond
    document.getElementById("achtergrond_" + afb).style.backgroundImage = 'url(Afbeeldingen/achtergrond_' + 1 + '.png)';

    // 2. keuze zitten is true
    keuze_zitten = true;

    // 3. ga verder met tekst
    volgendeTekst();

    // 4. speel trieste muziek
    speelTriestofBoosGeluid(triestGeluid);

}

function keuzeBoos() {

    // de keuze "Word boos op servicebalie" wordt toegevoegd aan het verhaal array
    verhaal.splice((45 + keuze), 0,
        "Ik draai mij onmiddelijk om naar de hulp-balie.",
        "\"WAT BETEKEND DIT?!\"",
        "\"Pardon?\"",
        "\"HOE BEDOEL JE JE BENT HET VLIEGTUIG KWIJT?!\"",
        "\"Ik snap ni...\"",
        "\"HOE KAN JE NOU EEN VLIEGTUIG KWIJT RAKEN?!\"",
        "\"Meneer, ik weet echt niet wat u bedoelt.\"",
        "\"ER STAAT HIER DAT CONTACT MET VLUCHT MH370 VERLOREN IS!\"",
        "\"Oh, maar...\"",
        "\"IK EIS EEN VERKLARING, WAT IS ER MET MIJN BROER GEBEURD!\"",
        "\"Meneer, het spijt me, maar ik weet ook verder niks over de status van vlucht MH370.\"",
        "\"ONZIN!\"",
        "Op dat moment voel ik dat ik bij mijn arm gegrepen wordt.",
        "Ik kijk naast me en zie dat er iemand van de bewaking mijn arm vast houdt.",
        "Op dat moment realiseer ik mij dat ik door de mensen om mij heen wordt aangekeken.",
        "Ik kijk terug naar de vrouw bij de hulp-balie en zie hoe geschrokken zij is.",
        "\"Sorry... het was niet mijn bedoeling om al mijn woede op u te uiten.\"",
        "Ik ga weer zitten en iedereen om mij heen gaat weer verder met wat zij aan het doen waren.",
        "Wacht eens er stond in nummer in dat bericht voor meer informatie."
    );
    keuze += 19;

    // keuze boos is true
    keuze_boos = true;

    // ga verder met tekst
    volgendeTekst();

    // 4. speel onheilspellende muziek
    speelTriestofBoosGeluid(onheilspellendGeluid);


}

function keuzeNieuws() {
    //laat de krant met het nieuws verschijnen
    document.getElementById("nieuws_bericht").style.display = 'inline';

    //nieuwsverhaal wordt toegevoegd aan het verhaal array
    verhaal.splice(23, 0,
        "Wow...",
        '"34 doden na aanval op treinstation China".',
        '"Het dodental als gevolg van het bloedbad op het station van de Chinese stad Kunming is opgelopen tot 34."',
        '"Zo\'n 130 mensen raakten gewond."',
        '"De autoriteiten hielden zondag separatistische Oeigoeren verantwoordelijk voor de aanslag."',
        '"In het zwart geklede mensen met lange messen doodden zaterdag zeker 29 voorbijgangers."',
        '"De politie op haar beurt doodde vier aanvallers, onder wie een vrouw. Een vijfde aanvaller is gewond aangehouden."',
        '"De politie is nog op zoek naar vijf andere daders."',
        '"Volgens president Xi Jinping zal alles worden gedaan om de \'gewelddadige terroristen\' op te sporen."',
        '"Ooggetuigen melden aan AP dat de daders iedereen die ze tegenkwamen neerstaken en geen doel voor ogen leken te hebben."',
        '"Hoewel over de daders eerst geen nadere mededelingen zijn gedaan,',
        '"liet de staatstelevisie in de nacht van zaterdag op zondag weten dat de daders komen uit de noordwestelijke regio van Xinjiang."',
        '"Hier wonen Oeigoeren, die in het verleden voor soortgelijke incidenten verantwoordelijk zijn gehouden."',
        '"De staatstelevisie repte over een \'gewelddadige terreuraanslag met voorbedachten rade\'."',
        "Oké, ik denk dat dat genoeg krant is voor één dag."
    );
    keuze += 15;

    // ga verder met de tekst
    volgendeTekst();
}

function keuzeWeer() {
    //laat de krant met het nieuws verschijnen
    document.getElementById("weerbericht").style.display = 'inline';

    //nieuwsverhaal wordt toegevoegd aan het verhaal array
    verhaal.splice(23, 0,
        "Laten we eens kijken hoe het weer eruit ziet.",
        "Dat lijkt mij best wel redelijk.",
        "Het ziet er in iedergeval niet uit alsof een vliegtuig daar veel problemen mee zou hebben.",
        "Gelukkig wordt het zonnig.",
        "Dat betekend dat de dag niet treurig kan worden.",
        "Het zorgt voor een rustig ritje terug naar huis en met een beetje geluk kunnen we ook nog op het terras gaan zitten bij de bar.",
        '"Ja", het wordt een mooie dag.'
    );
    keuze += 7;

    // ga verder met de tekst
    volgendeTekst();
}

function volgendeTekst() {

    // de volgende regel tekst wordt uit het tekstBlok gehaald
    var volgendeRegel = tekstBlok(1, keuze);

    // daarna wordt het regelID aangepast
    document.getElementById("midden_tekst_" + regelID).id = "midden_tekst_" + (regel + 1);
    regelID++;

    // dan wordt die regel gevuld en getoond met de regel uit het verhaal
    document.getElementById("midden_tekst_" + regelID).innerHTML = volgendeRegel;
}

function vorigeTekst() {

    // de vorige regel tekst wordt uit het tekstBlok gehaald
    var vorigeRegel = tekstBlok(-1);

    // daarna wordt het regelID aangepast
    document.getElementById("midden_tekst_" + regelID).id = "midden_tekst_" + (regel + 1);
    regelID--;

    // dan wordt die regel gevuld en getoond met de regel uit het verhaal
    document.getElementById("midden_tekst_" + regelID).innerHTML = vorigeRegel;
}

var zwartScherm = function () {

    // 1. pagina doet fadeout naar een zwart-scherm
    document.querySelector(DOMstrings.voorgrond).style.backgroundColor = "rgba(0, 0, 0, 1)";

    // 2. tekst in het midden van de pagina, met situatie omschrijving
    setTimeout(function () {
        document.getElementById("eerste").style.color = "rgba(255, 255, 255, 1)";
    }, 2000);

};

var zwartSchermWeg = function () {

    // 1. tekst onzichtbaar maken.
    document.getElementById("eerste").style.color = "rgba(255, 255, 255, 0)";
    document.getElementById("tweede").style.color = "rgba(255, 255, 255, 0)";

    // 2. zwarte achtergrond doorzichtig maken.
    setTimeout(function () {
        document.querySelector(DOMstrings.voorgrond).style.backgroundColor = "rgba(0, 0, 0, 0)";
    }, 1500);

};

var ogenOpenDicht = function () {
    document.querySelector(DOMstrings.verderTerugContainer).style.display = 'none';
    setTimeout(ogenSluiten, 1000);
    setTimeout(ogenOpen, 5000);
    setTimeout(function () {
        document.querySelector(DOMstrings.verderTerugContainer).style.display = 'inline';
        document.getElementById("ooglid_boven").style.display = 'none';
        document.getElementById("ooglid_onder").style.display = 'none';
    }, 6000);
};

var ogenSluiten = function () {
    document.getElementById("ooglid_boven").style.top = "-0%";
    document.getElementById("ooglid_onder").style.bottom = "-0%";
    document.getElementById("ooglid_boven").style.position = "absolute";
    document.getElementById("ooglid_onder").style.position = "absolute";
    setTimeout(function () {
        document.getElementById("ooglid_boven").style.backgroundColor = "rgba(0, 0, 0, 1)";
        document.getElementById("ooglid_onder").style.backgroundColor = "rgba(0, 0, 0, 1)";
        speelAchtergrondGeluid();
    }, 1000);
};

var ogenOpen = function () {
    document.getElementById("ooglid_boven").style.top = "-100%";
    document.getElementById("ooglid_onder").style.bottom = "-100%";
    speelAchtergrondGeluid();
    volgendeTekst();
    setTimeout(function () {
        document.getElementById("ooglid_boven").style.backgroundColor = "rgba(0, 0, 0, 0.6)";
        document.getElementById("ooglid_onder").style.backgroundColor = "rgba(0, 0, 0, 0.6)";
    }, 1000);
};

var bekijkBericht = function () {
    document.querySelector(DOMstrings.bericht).style.height = "90%";
    document.querySelector(DOMstrings.bericht).style.width = "84%";
    document.querySelector(DOMstrings.bericht).style.top = "50%";
    document.querySelector(DOMstrings.verderTerugContainer).style.display = 'inline';
};

var cancelBericht = function () {
    document.querySelector(DOMstrings.bericht).style.height = "15%";
    document.querySelector(DOMstrings.bericht).style.width = "13%";
    document.querySelector(DOMstrings.bericht).style.top = "30%";
};

var dagNachtOvergang = function () {
    // "verder" en "terug" knoppen verdwijnen
    document.querySelector(DOMstrings.verderTerugContainer).style.display = 'none';

    // Ochtend wordt middag
    setTimeout(function () {
        volgendeAchtergrond();
        volgendeTekst();
    }, 2000);

    // Middag wordt avond
    setTimeout(function () {
        volgendeAchtergrond();
        volgendeTekst();
    }, 6000);

    // Avond naar ochtend
    setTimeout(function () {
        volgendeAchtergrond();
        volgendeTekst();
    }, 10000);

    // 
    setTimeout(function () {
        document.querySelector(DOMstrings.verderTerugContainer).style.display = 'inline';
        speelAchtergrondGeluid();
    }, 11000);
};

var tweedeZwartScherm = function () {

    // 1. verwijder tekstblok en "verder" knop
    document.querySelector(DOMstrings.tekstBlok).style.display = 'none';
    document.querySelector(DOMstrings.verderTerugContainer).style.display = 'none';

    // 2. start samenvatting video
    document.querySelector(DOMstrings.nieuws).style.opacity = 1;
    document.querySelector(DOMstrings.nieuws).play();
};

function tweedeZwartSchermVervolg() {
    // 3. laat tekst zien met situatie beschrijving en verander ondertussen de achtergrond
    setTimeout(function () {
        document.querySelector(DOMstrings.zwartScherm2).style.opacity = '0';
        document.getElementById("tweede").style.color = "rgba(255, 255, 255, 1)";
        volgendeAchtergrond();
    }, 2000);

    // 4. zwarte scherm weg
    setTimeout(zwartSchermWeg, 5000);

    // 5. tekstblok en "verder" knop weer terug
    setTimeout(function () {
        document.querySelector(DOMstrings.tekstBlok).style.display = 'inline';
        document.querySelector(DOMstrings.verderTerugContainer).style.display = 'inline';
        document.querySelector(DOMstrings.zwartScherm2).style.display = 'none';
    }, 7000);
}

// Nieuws video
function playPause() {
    if (play) {
        document.querySelector(DOMstrings.nieuws).pause();
        document.querySelector(DOMstrings.pauze).style.opacity = 1;
        play = false;
    } else {
        document.querySelector(DOMstrings.nieuws).play();
        document.querySelector(DOMstrings.pauze).style.opacity = 0;
        play = true;
    }
}

function speelAchtergrondGeluid() {

    var id = setInterval(frame, 70);

    function frame() {
        if (!afspelen) {
            achtergrondGeluid.play();
            if (achtergrondGeluid.volume >= 0.20) {
                clearInterval(id);
                afspelen = true;
            } else {
                achtergrondGeluid.volume += 0.01;
            }
        } else {
            if (achtergrondGeluid.volume <= 0.01) {
                clearInterval(id);
                achtergrondGeluid.pause();
                afspelen = false;
            } else {
                achtergrondGeluid.volume -= 0.01;
            }
        }
    }

}

function speelTriestofBoosGeluid(muziek) {

    var id = setInterval(frame, 80);

    function frame() {
        if (!tweedeGeluid) {
            muziek.play();
            if (muziek.volume >= 0.30) {
                clearInterval(id);
                tweedeGeluid = true;
            } else {
                muziek.volume += 0.01;
            }
        } else {
            if (muziek.volume <= 0.01) {
                clearInterval(id);
                muziek.pause();
                tweedeGeluid = false;
            } else {
                muziek.volume -= 0.01;
            }
        }
    }

}

function start() {
    // 1. Eerste zwarte scherm + zet de startknop en credits knop uit
    zwartScherm();
    document.querySelector(DOMstrings.startBtn).disabled = true;
    //document.querySelector(DOMstrings.creditsBtn).disabled = true;

    // 2. Verwijder alles van het startscherm
    setTimeout(verwijderStart, 3000);

    // 3. Verander de achtergrond
    setTimeout(volgendeAchtergrond, 6000);
    setTimeout(zwartSchermWeg, 6000);

    // 4. start de functie: volgendeTekst() en geef het tekstblok weer
    setTimeout(volgendeTekst, 7000);
    setTimeout(speelAchtergrondGeluid, 7000);
    setTimeout(function () {
        document.querySelector(DOMstrings.tekstBlok).style.display = 'inline';
    }, 11000);

    // 5. de "verder" knop komt tevoorschijn (de "terug" knop nog niet want je kan niet terug op de eerste regel)
    setTimeout(function () {
        document.querySelector(DOMstrings.verderTerugContainer).style.display = 'inline';
        document.getElementById("terug_btn").style.display = 'none';
        verheldering();
    }, 11000);
}

var verwijderStart = function () {
    document.querySelector(DOMstrings.titellabel).style.display = 'none';
    document.querySelector(DOMstrings.startBtn).style.display = 'none';
};

function stopTriestBoosGeluid() {
    // * de muziek die bij de keuze komt stoppen (dit moet voor Apple gebruikers via een event listener...)
    document.getElementById("verder_btn").addEventListener("click", function () {
        triestGeluid.pause();
        onheilspellendGeluid.pause();
    });
}
