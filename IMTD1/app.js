// Zie welk tabblad actief is
function actieveTab(evt, pagina) {
    var i, tabblad, index;
    
    tabblad = document.getElementsByClassName("tabblad");
    for (i = 0; i < tabblad.length; i++) {
        tabblad[i].style.opacity = 0;
        tabblad[i].style.height = 0;
        tabblad[i].style.overflow = "hidden";
    }
    
    index = document.getElementsByClassName("index");
    for (i = 0; i < index.length; i++) {
        index[i].className = index[i].className.replace(" active", "");
    }
    
    // Fade in
    document.getElementById(pagina).style.opacity = 1;
    document.getElementById(pagina).style.height = "auto";
    document.getElementById(pagina).style.overflow = "visible";
    
    evt.currentTarget.className += " active";
    
    // Begin bovenaan de pagina wanneer je op een nieuw tabblad klikt
    scroll(0,0);
}

// Verander de stijl van de navigatie bar bij het scrollen
window.onscroll = function () {scrollFuntie(); };
function scrollFuntie() {
    var navi = document.getElementById("navi");
    if (this.scrollY <= 10) {
        navi.className = '';
    } else {
        navi.className = 'navi_scroll';
    }
}

// Bericht verstuurd
function verstuurd() {
    var popUp = document.querySelector(".pop-up");
    popUp.style.height = "50px";
    setTimeout(function() {
        popUp.style.height = 0;
    }, 2000);
}

// Begin bovenaan de pagina
setTimeout(function() {
    scroll(0,0);
}, 250);