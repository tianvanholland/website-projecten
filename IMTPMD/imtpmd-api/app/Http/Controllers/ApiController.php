<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sport;
use App\Video;

class ApiController extends Controller
{
  public function sport() {
    $allSports = Sport::all();
    return response()->json($allSports);
  }

  public function gifUrl($getGif) {
    return view("gif")->with('gif',$getGif);
  }
}
