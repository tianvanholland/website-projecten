<?php

use Illuminate\Database\Seeder;

class SportsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sports')->insert([
          'exercise' => 'Push-up',
          'description' => 'Lig op de grond met de handen iets breder dan de schouderbreedte. Verhoog het lichaam van de vloer door de armen met het lichaam recht uit te trekken. Houd het lichaam recht terwijl je zakt door je armen te buigen. Duw het lichaam weer omhoog door je armen uit te strekken.',
          'interval_time' => 5,
          'sport_video' => '/sport_videos/BWPushupWide.gif',
        ]);
        DB::table('sports')->insert([
          'exercise' => 'Squat',
          'description' => 'Hurk naar beneden door de heupen naar achteren te buigen en de knieën enigszins naar voren te buigen, waarbij je je lichaam recht houdt en de knieën in dezelfde richting wijzen als de voeten. Daal af tot de dijen net evenwijdig aan de vloer zijn gepasseerd. Hurk omhoog door de knieën en heupen te strekken totdat de benen recht zijn. Belangrijk is het om je hoofd naar voren te houden, je rug recht, je borst omhoog en je voeten plat op de grond te houden. Door de armen naar voren te houden kan het torso beter recht blijven.',
          'interval_time' => 4,
          'sport_video' => '/sport_videos/BWSquat.gif',
        ]);
        DB::table('sports')->insert([
          'exercise' => 'Plank',
          'description' => 'Plaats de onderarmen op de grond met je ellebogen gebogen in een hoek van 90 graden, uitgelijnd onder je schouders, met uw armen parallel op schouderbreedte. Plaats je voeten naast elkaar en raak alleen met je tenen de vloer aan. Til je buik van de grond en vorm een rechte lijn van je hielen naar de kruin van je hoofd. Houd deze positie vast.',
          'interval_time' => 30,
          'sport_video' => '/sport_videos/plank-static.gif',
        ]);
        DB::table('sports')->insert([
          'exercise' => 'Side-Plank left',
          'description' => 'Plaats de linker onderarm op de mat onder de schouder. Rust het rechterbeen op het linkerbeen en zet je knieën en heupen recht. Probeer je heupen zo hoog mogelijk te houden.',
          'interval_time' => 30,
          'sport_video' => '/sport_videos/sideplank.jpg',
        ]);
        DB::table('sports')->insert([
          'exercise' => 'Side-Plank right',
          'description' => 'Plaats de rechter onderarm op de mat onder de schouder. Rust het linkerbeen op het rechterbeen en zet je knieën en heupen recht. Probeer je heupen zo hoog mogelijk te houden.',
          'interval_time' => 30,
          'sport_video' => '/sport_videos/sideplank.jpg',
        ]);
        DB::table('sports')->insert([
          'exercise' => 'Diamond Push-up',
          'description' => 'Plaats je handen onder je borst en maak een diamantvorm met je handen (zorg dat je wijsvingers en duimen elkaar aanraken). Zak naar beneden door je armen te buigen en weer opmhoog door je armen te strekken, zorg ervoor dat je ellebogen niet te veel naar buiten wijzen.',
          'interval_time' => 5,
          'sport_video' => '/sport_videos/DiamondPushUp.gif',
        ]);
        DB::table('sports')->insert([
          'exercise' => 'Sit up',
          'description' => 'Lig plat op je rug met je knieën gebogen en je voeten plat op de vloer vastgeklemd zodat ze op de vloer blijven tijdens de oefening. Plaats je handen achter of aan de zijkant van je nek. Til je torso van de grond door de taille en de heupen te buigen. Ga terug tot de achterkant van de schouders de grond raken en herhaal.',
          'interval_time' => 5,
          'sport_video' => '/sport_videos/BWSitupNeck.gif',
        ]);
        DB::table('sports')->insert([
          'exercise' => 'Wall Sit',
          'description' => 'Ga met je rug tegen een muur staan. Zak rustig door je knieën, zet je voeten een stapje naar voren, een heupbreedte uit elkaar. Zorg dat je knieën in een hoek van 90 graden staan en houd vol.',
          'interval_time' => 30,
          'sport_video' => '/sport_videos/WallSit.jpg',
        ]);
        DB::table('sports')->insert([
          'exercise' => 'Crunches',
          'description' => 'Lig plat op je rug met je knieën gebogen en je voeten plat op de vloer ongeveer één voet afstand van je onderrug. Plaats je vingertoppen tegen de zijkant van je hoofd met je handpalmen naar buiten. Trek je buik in in de basis van je wervelkolom om de spieren te activeren en til vervolgens je hoofd en schouders van de grond. Keer daarna terug naar de beginpositie en herhaal.',
          'interval_time' => 6,
          'sport_video' => '/sport_videos/Crunch-Side.gif',
        ]);
        DB::table('sports')->insert([
          'exercise' => 'Laying Leg Raises',
          'description' => 'Lig op je rug met je handen op de vloer aan beide kanten. Houd de benen bij elkaar en zo recht mogelijk. Til je benen omhoog tot een 90 graden hoek, of zo hoog mogelijk en houd ze hier voor 1 seconde. Daarna laat je ze weer langzaam zakken.',
          'interval_time' => 7,
          'sport_video' => '/sport_videos/LegRaises-Side.gif',
        ]);
        DB::table('sports')->insert([
          'exercise' => 'Glute Bridge',
          'description' => 'Ga op je rug liggen. Pin je schouderbladen vast in de mat en breng je heup omhoog. Span je buik- en bilspieren aan en adem in. Breng je heup omhooog en kom terug in de uitgangspositie en herhaal.',
          'interval_time' => 5,
          'sport_video' => '/sport_videos/GluteBridges-Side.gif',
        ]);
        DB::table('sports')->insert([
          'exercise' => 'Superman',
          'description' => 'Lig met je buik op de mat met je benen naast elkaar en je armen uitgestrekt. Breng het bovenlichaam en de benen langzaam van de vloer, probeer ze zo hoog mogelijk te krijgen en hou dat even vast. Daarna breng je het bovenlichaam en de benen langzaam terug naar de vloer en herhaal.',
          'interval_time' => 5,
          'sport_video' => '/sport_videos/Superman.gif',
        ]);
        DB::table('sports')->insert([
          'exercise' => 'Bench Dips',
          'description' => 'Pak de rand van de bank vast met je handen, houd je voeten bij elkaar en je benen recht. Verlaag je lichaam recht naar beneden, ga daarna weer langzaam omhoog naar de startpositie en herhaal.',
          'interval_time' => 4,
          'sport_video' => '/sport_videos/male-bench-tricep-dip-side.gif',
        ]);
    }
}
