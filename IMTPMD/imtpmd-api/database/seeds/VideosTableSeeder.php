<?php

use Illuminate\Database\Seeder;

class VideosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('videos')->insert([
        'video' => '/sport_videos/BWPushupWide.gif',
      ]);
      DB::table('videos')->insert([
        'video' => '/sport_videos/BWSquat.gif',
      ]);
      DB::table('videos')->insert([
        'video' => '/sport_videos/plank-static.gif',
      ]);
      DB::table('videos')->insert([
        'video' => '/sport_videos/sideplank.jpg',
      ]);
      DB::table('videos')->insert([
        'video' => '/sport_videos/DiamondPushUp.gif',
      ]);
      DB::table('videos')->insert([
        'video' => '/sport_videos/BWSitupNeck.gif',
      ]);
      DB::table('videos')->insert([
        'video' => '/sport_videos/WallSit.jpg',
      ]);
      DB::table('videos')->insert([
        'video' => '/sport_videos/Crunch-Side.gif',
      ]);
      DB::table('videos')->insert([
        'video' => '/sport_videos/LegRaises-Side.gif',
      ]);
      DB::table('videos')->insert([
        'video' => '/sport_videos/GluteBridges-Side.gif',
      ]);
      DB::table('videos')->insert([
        'video' => '/sport_videos/Superman.gif',
      ]);
      DB::table('videos')->insert([
        'video' => '/sport_videos/male-bench-tricep-dip-side.gif',
      ]);
    }
}
