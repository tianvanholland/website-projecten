<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         Schema::create('sports', function (Blueprint $table) {
             $table->bigIncrements('id');
             $table->String('exercise');
             $table->Text('description');
             $table->integer('interval_time');

             $table->String('sport_video');

             $table->foreign('sport_video')->references('video')->on('videos')->onDelete('cascade');
           });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('sports');
     }
}
