<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>{{$gif}}</title>
    <style>
    html, body {
      margin: 0;
      display: block;
      background: #0e0e0e;
      height: 100%;
      text-align: center;
    }
    img {
      -webkit-user-select: none;
      margin: 0;
      top: 50%;
      left: 50%;
      margin-right: -50%;
      transform: translate(-50%, -50%);
      position: absolute;
    }
    </style>
  </head>
  <body>
    <img src="/sport_videos/{{$gif}}" alt="Kon afbeelding niet laden"/>
  </body>
</html>
