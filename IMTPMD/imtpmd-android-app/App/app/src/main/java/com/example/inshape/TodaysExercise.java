package com.example.inshape;

import android.arch.persistence.room.Room;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


public class TodaysExercise extends AppCompatActivity {
    private static final String TAG = "Log.TodaysExercise";
    private boolean toTraining;
    private AppDatabase db;
    private ExerciseInfo exerciseInfo;
    private TextView progressTextView;
    private String exercise;
    private String date;

    // functie die zorgt dat de progress direct upgedate wordt (dit gebeurde niet bij als je terug kwam vanuit TrainingActivity
    @Override
    public void onResume(){
        super.onResume();
        exerciseInfo = db.exerciseInfoDAO().getExerciseInfo(date, exercise);
        progressTextView.setText("Voortgang: " + exerciseInfo.getProgress());
    }

    @Override
    public void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todays_exercise);

        db = Room // Start de lokale database
                .databaseBuilder(getApplicationContext(),AppDatabase.class,"in_shape_db")
                .allowMainThreadQueries() //NIET IN PRODUCTIE!!
                .build();
        exercise = getExtras("exercise");
        date = getExtras("date");
        try {
            toTraining = getIntent().getExtras().getBoolean("toTraining");
        } catch(Exception e) {
            Log.d(TAG, "exception", e);
        }
        exerciseInfo = db.exerciseInfoDAO().getExerciseInfo(date, exercise);

        // haal de elementen op uit de xml
        TextView exerciseTextView = findViewById(R.id.exerciseTextView);
        TextView goalTextView = findViewById(R.id.goalTextView);
        progressTextView = findViewById(R.id.progressTextView);
        Button toTrainingButton = findViewById(R.id.toTrainingButton);
        Button deleteButton = findViewById(R.id.deleteButton);
        ImageView exit = findViewById(R.id.todaysExit);

        exerciseTextView.setText(exercise);
        progressTextView.setText("Voortgang: " + exerciseInfo.getProgress());
        goalTextView.setText("Doel: " + exerciseInfo.getGoal());

        // verlaat deze activity als je op het kruisje klikt
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        // open AlertDialog zodat je kan kiezen tussen ja en nee
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(TodaysExercise.this);

                // Titel voor alert dialog
                builder.setTitle("Maak uw keuze.");

                // Ask the final question
                builder.setMessage("Weet u zeker dat u deze oefening wilt verwijderen?");

                // deze functie wordt uitgevoerd als er op ja geklikt wordt
                builder.setPositiveButton("Ja", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        db.exerciseInfoDAO().delete(exerciseInfo);
                        Toast.makeText(getApplicationContext(),"Oefening verwijderd",Toast.LENGTH_SHORT).show();
                        finish();
                    }
                });

                // deze functie wordt uitgevoerd als er op nee geklikt wordt
                builder.setNegativeButton("Nee", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) { }
                });

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        toTrainingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // toTraining is true als de date vandaag is want alleen dan mag je de oefening doen
                if(toTraining) {
                    // ga naar de TrainingActivity en geef de primary key van ExerciseInfo mee
                    Intent i = new Intent(TodaysExercise.this, TrainingActivity.class);
                    i.putExtra("exercise", exercise);
                    i.putExtra("date", date);
                    startActivity(i);
                } else {
                    Toast.makeText(TodaysExercise.this, "Deze oefening is niet ingepland voor vandaag!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    // haal de date en exercise op om zo alle data te kunnen opvragen uit de room db
    public String getExtras(String extra){
        Bundle extras = getIntent().getExtras();
        if(extras !=null)
        {
            return(extras.getString(extra));
        }
        return null;
    }
}
