package com.example.inshape;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

@Database(entities = {Sport.class, ExerciseInfo.class}, version = 3)
public abstract class AppDatabase extends RoomDatabase {
    public abstract SportDAO sportDAO();
    public abstract ExerciseInfoDAO exerciseInfoDAO();
}
