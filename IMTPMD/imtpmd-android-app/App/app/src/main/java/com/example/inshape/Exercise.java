package com.example.inshape;


import com.google.gson.annotations.SerializedName;


public class Exercise {
    @SerializedName("exercise")
    public String exercise;
    @SerializedName("description")
    public String description;
    @SerializedName("interval_time")
    public int intervalTime;
    @SerializedName("sport_video")
    public String sportVideo;

    public String getDescription() {
        return description;
    }

    public int getIntervalTime() {
        return intervalTime;
    }

    public String getSportVideo() {
        return sportVideo;
    }

    public String getExercise() {
        return this.exercise;
    }
}