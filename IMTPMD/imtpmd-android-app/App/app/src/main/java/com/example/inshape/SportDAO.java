package com.example.inshape;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface SportDAO {
    @Query("SELECT * FROM sport")
    List<Sport> loadAll();

    @Query("SELECT * FROM sport WHERE exercise = :exercise")
    Sport getExercise(String exercise);

    @Insert
    void insertAll(Sport... sports);

    @Delete
    void delete(Sport sport);
}