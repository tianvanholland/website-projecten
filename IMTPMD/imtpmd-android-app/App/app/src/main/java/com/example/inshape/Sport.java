package com.example.inshape;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity
public class Sport {
    // De componenten in de lokale database
    @PrimaryKey
    @NonNull
    @ColumnInfo (name = "exercise")
    private String exercise;
    @ColumnInfo (name = "description")
    private String description;
    @ColumnInfo (name = "interval_time")
    private int intervalTime;
    @ColumnInfo (name = "sports_video")
    private String sportVideo;

    public Sport(){}

    // getters en setters
    public String getExercise() {
        return exercise;
    }

    public void setExercise(String exercise) {
        this.exercise = exercise;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getIntervalTime() {
        return intervalTime;
    }

    public void setIntervalTime(int intervalTime) {
        this.intervalTime = intervalTime;
    }

    public String getSportVideo() {
        return sportVideo;
    }

    public void setSportVideo(String sportVideo) {
        this.sportVideo = sportVideo;
    }
}



