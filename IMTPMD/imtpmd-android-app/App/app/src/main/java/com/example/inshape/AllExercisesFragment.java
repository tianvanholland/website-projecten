package com.example.inshape;

import android.arch.persistence.room.Room;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class AllExercisesFragment extends Fragment {


    // code voor het eerste tabblad
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab1_all_exercises_fragment, container, false);

        String text;
        AppDatabase db = Room // Start de lokale database
                .databaseBuilder(getActivity().getApplicationContext(),AppDatabase.class,"in_shape_db")
                .allowMainThreadQueries() //NIET IN PRODUCTIE!!
                .build();

        final TextView header = (TextView) view.findViewById(R.id.tab1TextView);

        final ListView allExercisesListView = (ListView) view.findViewById(R.id.allExercisesListView);
        final ArrayList<String> allExercises = new ArrayList<>();
        final List<Sport> sportList = db.sportDAO().loadAll();
        if(sportList.size() == 0){
            text = "Verbind eenmalig met het internet en herstart de app om de oefeningen in te laden";
        } else {
            text = "Kies een oefening:";
            for (int i = 0; i < sportList.size(); i++) {
                allExercises.add(sportList.get(i).getExercise());
            }
        }
        header.setText(text);

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, allExercises);
        allExercisesListView.setAdapter(arrayAdapter);

        // als op een item geklikt word komt er een popup (nieuwe activity) waar de juiste oefening, video en description worden meegegeven
        allExercisesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String exercise = allExercises.get(position);
                String video = sportList.get(position).getSportVideo();
                String description = sportList.get(position).getDescription();
                String interval = String.valueOf(sportList.get(position).getIntervalTime());
                Intent i = new Intent(getActivity(), ExercisePop.class);
                i.putExtra("exercise", exercise);
                i.putExtra("video", video);
                i.putExtra("description", description);
                i.putExtra("interval", interval);
                startActivity(i);
            }
        });
        return view;
    }
}
