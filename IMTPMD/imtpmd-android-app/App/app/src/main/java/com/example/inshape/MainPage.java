package com.example.inshape;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

public class MainPage extends AppCompatActivity {

    // dit is de blueprint van een fragment
    private SectionsPageAdapter sectionsPageAdapter;

    private ViewPager viewPager;
    @Override
    public void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mainpage);

        sectionsPageAdapter = new SectionsPageAdapter(getSupportFragmentManager());

        viewPager = findViewById(R.id.container);
        setupViewPage(viewPager);

        TabLayout tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setCurrentItem(1);
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        int item = viewPager.getCurrentItem();
        setupViewPage(viewPager);
        viewPager.setCurrentItem(item);
    }

    // stuur de fragments naar de blueprint en maak de tabbladen aan
    private void setupViewPage(ViewPager viewPager) {
        SectionsPageAdapter adapter = new SectionsPageAdapter(getSupportFragmentManager());
        adapter.addFragment(new AllExercisesFragment(), "Alle oefeningen");
        adapter.addFragment(new ExerciseSheduleFragment(), "Vandaag");
        adapter.addFragment(new CalendarFragment(), "Kalender");
        viewPager.setAdapter(adapter);
    }
}
