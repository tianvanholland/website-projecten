package com.example.inshape;

import android.arch.persistence.room.Room;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ExerciseSheduleFragment extends Fragment {


    private static final String TAG = "Log.ExerciseShedule";

    // code voor het derde tabblad
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab2_exercise_shedule_fragment, container, false);

        // Haal database op zodat deze gebruikt kan worden in fragment
        AppDatabase db = Room // Start de lokale database
                .databaseBuilder(getActivity().getApplicationContext(),AppDatabase.class,"in_shape_db")
                .allowMainThreadQueries() //NIET IN PRODUCTIE!!
                .build();

        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        month = month + 1;
        final String today = year + "/" + month + "/" + day;

        ListView todaysListView = (ListView) view.findViewById(R.id.todaysListView);
        final ArrayList<String> todaysExercises = new ArrayList<>();

        final List<ExerciseInfo> todaysList = db.exerciseInfoDAO().loadAllByDate(today);
        for (int i = 0; i < todaysList.size(); i++) {
            if(todaysList.get(i).getProgress() == todaysList.get(i).getGoal()) {
                todaysExercises.add(todaysList.get(i).getExercise() + ": " + todaysList.get(i).getProgress() + " / " + todaysList.get(i).getGoal() + ", Goed gedaan!");
            } else {
                todaysExercises.add(todaysList.get(i).getExercise() + ": " + todaysList.get(i).getProgress() + " / " + todaysList.get(i).getGoal());

            }
        }

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, todaysExercises){
            @Override
            public View getView(int position, View convertView, ViewGroup parent){
                // Get the current item from ListView
                TextView tv = (TextView) super.getView(position, convertView, parent);
                if(todaysList.get(position).getProgress() == todaysList.get(position).getGoal())
                {
                    // Verander de tekst kleur wanneer de oefening voltooid is
                    tv.setTextColor(Color.parseColor("#00ff00"));
                }
                return tv;
            }
        };

        todaysListView.setAdapter(arrayAdapter);

        todaysListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // check of de oefening niet al behaald is:
                if (todaysList.get(position).getProgress() == todaysList.get(position).getGoal()) {
                    // deze training kan je niet meer doen, hij is al voltooid
                    Toast.makeText(getActivity(), "Deze oefening is al voltooid", Toast.LENGTH_SHORT).show();
                } else {
                    // Ga naar de volgende Activity met de gegevens van de geselecteerde oefening
                    String s = todaysExercises.get(position);
                    String exercise = s.substring(0, s.indexOf(":")); // haal de progress en goal van de string af
                    Intent i = new Intent(getActivity(), TodaysExercise.class); // verander naar de nieuwe activity
                    i.putExtra("exercise", exercise);
                    i.putExtra("date", today);
                    i.putExtra("toTraining", true);
                    startActivity(i);
                }
            }
        });
        return view;
    }
}
