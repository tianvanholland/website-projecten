package com.example.inshape;

import android.arch.persistence.room.Room;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CalendarView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class CalendarFragment extends Fragment {

    private String selectedDate;
    private TextView calendarTextView;
    private ListView calendarListView;
    private AppDatabase db;
    private String maand;
    private boolean toTraining;
    private String text;

    // code voor het derde tabblad
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab3_calendar_fragment, container, false);

        db = Room // Start de lokale database
            .databaseBuilder(getActivity().getApplicationContext(),AppDatabase.class,"in_shape_db")
            .allowMainThreadQueries() //NIET IN PRODUCTIE!!
            .build();

        calendarListView = view.findViewById(R.id.calendarListView);
        calendarTextView = view.findViewById(R.id.calendarTextView);

        // Krijg de huidige datum
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        month = month + 1;
        final String today = year + "/" + month + "/" + day;
        selectedDate = today;

        changeDate(day, month, today);

        // Kalender om de datum te veranderen
        CalendarView calendarView = view.findViewById(R.id.calendarView);
        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month,
                                            int dayOfMonth) {
                selectedDate = year + "/" + (month + 1) + "/" + dayOfMonth;
                changeDate(dayOfMonth, (month + 1), today);
            }
        });

        return view;
    }
    // Functie voor de veranderde datum, het haald de oefeningen die op die dag geboekt staan
    private void changeDate(int day, int month, final String today) {
        switch (month) {
            case 1: maand = "Januari";
            break;
            case 2: maand = "Februari";
            break;
            case 3: maand = "Maart";
            break;
            case 4: maand = "April";
            break;
            case 5: maand = "Mei";
            break;
            case 6: maand = "Juni";
            break;
            case 7: maand = "Juli";
            break;
            case 8: maand = "Augustus";
            break;
            case 9: maand = "September";
            break;
            case 10: maand = "Oktober";
            break;
            case 11: maand = "November";
            break;
            case 12: maand = "December";
            break;
        }

        // pas de tekst aan die onder de kalender staat

        // Vul de listview met oefeningen die op die datum geboekt zijn
        final ArrayList<String> todaysExercises = new ArrayList<>();
        final List<ExerciseInfo> todaysList = db.exerciseInfoDAO().loadAllByDate(selectedDate);
        if(todaysList.size() == 0){
            text = "Geen oefeningen geboekt voor: \n" + day + " " + maand;
        } else {
            text = "Oefeningen geboekt voor: \n" + day + " " + maand;
            for (int i = 0; i < todaysList.size(); i++) {
                todaysExercises.add(todaysList.get(i).getExercise() + ": " + todaysList.get(i).getGoal() + " Keer.");
            }
        }

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, todaysExercises){
            @Override
            public View getView(int position, View convertView, ViewGroup parent){
                // Get the current item from ListView
                TextView tv = (TextView) super.getView(position, convertView, parent);
                if(todaysList.get(position).getProgress() == todaysList.get(position).getGoal())
                {
                    // Verander de tekst kleur wanneer de oefening voltooid is
                    tv.setTextColor(Color.parseColor("#00ff00"));
                } else {
                    tv.setTextColor(getResources().getColor(R.color.colorWhite));
                }
                return tv;
            }
        };

        calendarTextView.setText(text);
        calendarListView.setAdapter(arrayAdapter);

        calendarListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (todaysList.get(position).getProgress() == todaysList.get(position).getGoal()) {
                    // deze training kan je niet meer doen, hij is al voltooid
                    Toast.makeText(getActivity(), "Deze oefening is al voltooid", Toast.LENGTH_SHORT).show();
                } else {
                    String s = todaysExercises.get(position);
                    String exercise = s.substring(0, s.indexOf(":")); // haal de progress en goal van de string af
                    if (selectedDate.equals(today)) {
                        toTraining = true;
                    } else {
                        toTraining = false;
                    }
                    Intent i = new Intent(getActivity(), TodaysExercise.class); // verander naar de nieuwe activity
                    i.putExtra("exercise", exercise);
                    i.putExtra("date", selectedDate);
                    i.putExtra("toTraining", toTraining);
                    startActivity(i);
                }
            }
        });
    }
}
