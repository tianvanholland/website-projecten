package com.example.inshape;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;



@Dao
public interface ExerciseInfoDAO {
    @Query("SELECT * FROM exerciseInfo")
    List<ExerciseInfo> loadAll();

    @Query("SELECT * FROM exerciseinfo WHERE date = :date")
    List<ExerciseInfo> loadAllByDate(String date);

    @Query("SELECT * FROM exerciseinfo WHERE ((date = :date) AND (exercise = :exercise))")
    ExerciseInfo getExerciseInfo(String date, String exercise);

    @Query("UPDATE exerciseinfo SET progress=:progress WHERE ((date = :date) AND (exercise = :exercise))")
    void updateRoomProgress(int progress, String date, String exercise);

    @Insert
    void insertAll(ExerciseInfo... exerciseInfo);

    @Delete
    void delete(ExerciseInfo exerciseInfo);
}