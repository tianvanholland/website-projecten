package com.example.inshape;

import android.arch.persistence.room.Room;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;

public class LandingPage extends AppCompatActivity {

    private static final String TAG = "LandingPage";

    @Override
    public void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landingpage);

        final AppDatabase db = Room // Start de lokale database
                .databaseBuilder(getApplicationContext(),AppDatabase.class,"in_shape_db")
                .allowMainThreadQueries() //NIET IN PRODUCTIE!!
                .build();

        // DOM elementen
        final LinearLayout landingPage = findViewById(R.id.landingPage);
        TextView titel = findViewById(R.id.titel);
        final TextView klikDoor = findViewById(R.id.klikDoor);

        // Animatie van titel die naar beneden valt
        TranslateAnimation animationTitel = new TranslateAnimation(0.0f, 0.0f, -800.0f, 0.0f);
        animationTitel.setDuration(1300);
        // Animatie aan titel gegeven
        titel.startAnimation(animationTitel);

        // na 2 seconden kan er op het scherm geklikt worden deze functie is hetzelfde als een SetTimeout in javascript
        new android.os.Handler().postDelayed(
                new Runnable() {
                    @Override
                    public void run() {
                        landingPage.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent i = new Intent(LandingPage.this, MainPage.class);
                                startActivity(i);
                            }
                        });
                    }
                },

        2000);

        // na 2 seconden komt de tekst op het scherm die zegt dat je op het scherm moet klikken
        new android.os.Handler().postDelayed(
                new Runnable() {
                    @Override
                    public void run() {
                        klikDoor.setVisibility(View.VISIBLE);
                    }
                },
                2000);

        

        final ArrayList<String> sports = new ArrayList<>();
        NetworkManager.getInstance(this).getRequest(
                "http://188.166.31.210/api/sports/",
                new VolleyCallback() {
                    @Override
                    public void onSucces(String result) {
                        Gson gson = new Gson();
                        Exercise[] exercise = gson.fromJson(result, Exercise[].class);
                        for (int i = 0; i < exercise.length; i++) {
                            try {
                                Sport s = new Sport();
                                s.setExercise(exercise[i].getExercise());
                                s.setDescription(exercise[i].getDescription());
                                s.setIntervalTime(exercise[i].getIntervalTime());
                                s.setSportVideo(exercise[i].getSportVideo());
                                db.sportDAO().insertAll(s);
                            } catch (Exception e) {
                                Log.d(TAG, "Deze oefening zit al in de room database en kan niet nogmaals worden toegevoegd");
                            }
                        }
                    }
                });
    }
}
