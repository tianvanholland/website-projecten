package com.example.inshape;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class SectionsPageAdapter extends FragmentPagerAdapter {

    private final List<Fragment> fragmentList = new ArrayList<>();
    private final List<String> fragmentTitleList = new ArrayList<>();

    // een fragment toevoegen aan de list om tabbladen van te maken
    public void addFragment(Fragment fragment, String title) {
        fragmentList.add(fragment);
        fragmentTitleList.add(title);
    }

    // constructor
    public SectionsPageAdapter(FragmentManager fm) {
        super(fm);
    }

    // geef de titel van het fragment
    @Override
    public CharSequence getPageTitle(int position) {
        return fragmentTitleList.get(position);
    }

    // geef het opgevraagde fragment
    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    // geef het aantal fragmenten die in de list staan
    @Override
    public int getCount() {
        return fragmentList.size();
    }
}
