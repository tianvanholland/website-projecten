package com.example.inshape;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

public class NetworkManager {

    //Singleton
    private static final String TAG = "NetworkManager";
    private static NetworkManager instance = null;

    public RequestQueue requestQueue;

    private NetworkManager(Context context) {
        requestQueue = Volley.newRequestQueue(context.getApplicationContext());
    }

    //Dit moet je aanvragen in het begin van je app om de Netwerkmanager aan te maken (dit kan maar 1 keer)
    public static synchronized NetworkManager getInstance(Context context) {
        if(instance == null) {
            instance = new NetworkManager(context);
        }
        return instance;
    }

    //Dit gebruik je als je de netwerkmanager wilt krijgen zonder context, natuurlijk moet hij dan wel al bestaan.
    public static synchronized NetworkManager getInstance(){
        if(instance == null) {
            throw new IllegalStateException("Hij bestaat niet, eerst getInstance met context aanroepen");
        }
        return instance;
    }

    // vraag de url/Api op, je zet hem in de queue zodat de vorige aanvraag niet verloren gaat
    public void getRequest(String prefixUrl, final VolleyCallback volleyCallback) {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, prefixUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                volleyCallback.onSucces(response);
            }
        }, new Response.ErrorListener() { // als je niks krijgt uit de url krijg je een foutmelding
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "oeps: (");
            }
        });
        requestQueue.add(stringRequest);
    }

}