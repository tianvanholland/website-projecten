package com.example.inshape;

import android.arch.persistence.room.Room;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.Locale;

import pl.droidsonroids.gif.GifImageView;

public class TrainingActivity extends AppCompatActivity {
    private static final String TAG = "Log.TrainingActivity";
    private static long START_TIME_IN_MILLIS;
    private TextToSpeech tts;
    private ExerciseInfo exerciseInfo;
    private Sport sport;

    private TextView titel;
    private GifImageView gifImageView;
    private TextView progress;
    private TextView timer;
    private Button pausePlayButton;
    private boolean trainingRunning;
    private CountDownTimer countDownTimer;
    private long timeLeft;
    private int intervalTime;
    private int updateProgressNumber;
    private AppDatabase db;
    private String exercise;
    private String date;
    private ImageView exit;

    @Override
    public void onPause(){
        pauseTraining();
        super.onPause();
    }

    @Override
    public void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_training);

        titel = findViewById(R.id.trainingTextView);
        gifImageView = findViewById(R.id.trainingGifImageView);
        progress = findViewById(R.id.progressTextView);
        timer = findViewById(R.id.timerTextView);
        pausePlayButton = findViewById(R.id.pause_play_button);
        intervalTime = -1;
        exit = findViewById(R.id.trainingExit);

        db = Room // Start de lokale database
                .databaseBuilder(getApplicationContext(),AppDatabase.class,"in_shape_db")
                .allowMainThreadQueries() //NIET IN PRODUCTIE!!
                .build();
        exercise = getExtras("exercise");
        date = getExtras("date");

        // DATE, EXERCISE, GOAL, PROGRESS
        exerciseInfo = db.exerciseInfoDAO().getExerciseInfo(date, exercise);

        // De dingen die in de API staan
        sport = db.sportDAO().getExercise(exercise);

        // verlaat deze acitivity als je op het kruisje klikt
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // stop de stem als ze aan het praten is
                try {
                    tts.stop();
                } catch (Exception e) {
                    Log.d(TAG, "exeption", e);
                }
                finish();
            }
        });

        // zet de volledige tijd voor de oefening in deze variabel
        START_TIME_IN_MILLIS = (((exerciseInfo.getGoal() - exerciseInfo.getProgress()) * sport.getIntervalTime()) * 1000) + 50;
        timeLeft = START_TIME_IN_MILLIS;
        updateProgressNumber = exerciseInfo.getProgress();

        // plaats de exercise in de titel met de juiste gif of afbeelding eronder
        titel.setText(exercise);
        Glide.with(this).load("http://188.166.31.210" + sport.getSportVideo()).onlyRetrieveFromCache(true).into(gifImageView);

        Log.d(TAG, exerciseInfo.getGoal()+ " " + sport.getIntervalTime() + "     " + START_TIME_IN_MILLIS);

        // start de oefening door af te tellen
        tts = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                tts.setLanguage(new Locale("nl_NL"));
                tts.setSpeechRate(0.75f);
                tts.speak("Klaar? 3, 2, 1, Start!", TextToSpeech.QUEUE_FLUSH, null);
            }
        });

        // de pauze knop zet alles op pauze klik er weer op om weer door te gaan
        pausePlayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (trainingRunning) {
                    pauseTraining();
                } else {
                    resumeTraining();
                }
            }
        });

        // zet de timer en de progress textView goed met de gebruikte info
        updateTimerText();
        progress.setText(exerciseInfo.getProgress() + "   /   " + exerciseInfo.getGoal());

        // laat de spreker uitpraten voordat de oefening begint
        new android.os.Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "START");
                resumeTraining();
            }
        }, 6000);
    }

    // start de training of ga verder met de training
    private void resumeTraining() {
        countDownTimer = new CountDownTimer(timeLeft, 1000) { // elke 1000 miliseconde gaat deze functie af
            @Override
            public void onTick(long millisUntilFinished) {
                timeLeft = millisUntilFinished;
                updateTimerText();
                intervalTime++;
                training();
            }

            @Override
            public void onFinish() { // Wanneer de timer op 0 komt te staan
                trainingRunning = false;
                updateTimerText();
                intervalTime++;
                training();
                pausePlayButton.setVisibility(View.INVISIBLE);

                new android.os.Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        tts = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
                            @Override
                            public void onInit(int status) {
                                tts.setLanguage(new Locale("nl_NL"));
                                tts.setSpeechRate(0.75f);
                                tts.speak("Goed gedaan!", TextToSpeech.QUEUE_FLUSH, null);
                            }
                        });
                        db.exerciseInfoDAO().updateRoomProgress(exerciseInfo.getGoal(), date, exercise);
                        Intent i = new Intent(TrainingActivity.this, MainPage.class); // Ga terug naar het vandaag tabblad
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                        finish();
                    }
                }, 1000);
            }
        }.start();

        trainingRunning = true;
        pausePlayButton.setText("PAUZE");
        pausePlayButton.setVisibility(View.VISIBLE);
    }

    // zet de training op pauze
    private void pauseTraining() {
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        trainingRunning = false;
        pausePlayButton.setText("GA VERDER");
    }

    // de timer wordt geupdate
    private void updateTimerText() {
        int minutes = (int) (timeLeft / 1000) / 60;
        int seconds = (int) (timeLeft / 1000) % 60;

        // een timer formaat
        String timeLeftFormatted = String.format(Locale.getDefault(),"%02d:%02d", minutes, seconds);

        timer.setText(timeLeftFormatted);
    }

    // functie die bij houdt wanneer de progressie opgeteld moet worden
    private void training() {
        if (intervalTime == sport.getIntervalTime()) {
            updateProgressNumber++;
            db.exerciseInfoDAO().updateRoomProgress(updateProgressNumber, date, exercise);
            exerciseInfo.setProgress(updateProgressNumber);
            Log.d(TAG, sport.getExercise() + " heeft progress van " + exerciseInfo.getProgress());
            updateProgress();
            intervalTime = 0;
        }
    }

    // de functie die de progressie ook daadwerkelijk update en de spreker die zegt hoever je bent
    private void updateProgress() {
        final String tellProgress = String.valueOf(exerciseInfo.getProgress());
        tts = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                tts.setLanguage(new Locale("nl_NL"));
                tts.setSpeechRate(0.85f);
                tts.speak(tellProgress, TextToSpeech.QUEUE_FLUSH, null);
            }
        });

        progress.setText(exerciseInfo.getProgress() + "   /   " + exerciseInfo.getGoal());
    }

    public String getExtras(String extra){
        Bundle extras = getIntent().getExtras();
        if(extras !=null)
        {
            return(extras.getString(extra));
        }
        return null;
    }
}