package com.example.inshape;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.support.annotation.NonNull;

@Entity(primaryKeys = {"exercise","date"})
public class ExerciseInfo {
    @NonNull
    @ColumnInfo(name = "exercise")
    private String exercise;

    //hier moet misschien een date type
    @NonNull
    @ColumnInfo(name = "date")
    private String date;

    @ColumnInfo(name = "goal")
    private int goal;
    @ColumnInfo(name = "progress")
    private int progress;

    public ExerciseInfo(){}

    public String getExercise() {
        return exercise;
    }

    public void setExercise(@NonNull String exercise) {
        this.exercise = exercise;
    }

    public int getGoal() {
        return goal;
    }

    public void setGoal(int goal) {
        this.goal = goal;
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date){
        this.date = date;
    }
}
