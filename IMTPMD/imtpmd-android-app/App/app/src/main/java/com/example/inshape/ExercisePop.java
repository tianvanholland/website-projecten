package com.example.inshape;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.arch.persistence.room.Room;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.Calendar;
import java.util.Locale;

import pl.droidsonroids.gif.GifImageView;

public class ExercisePop extends Activity {
    private static final String TAG = "Log.ExercisePop";
    private DatePickerDialog.OnDateSetListener dateSetListener;
    private TextToSpeech tts;

    protected void onPause() {
        try {
            tts.stop();
        } catch (Exception e) {
            Log.d(TAG, "exeption", e);
        }
        super.onPause();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise_pop);

        Button addButton = findViewById(R.id.addButton);
        TextView exerciseTextView = findViewById(R.id.exerciseTextView);
        GifImageView exerciseGifImageView = findViewById(R.id.exerciseGifImageView);
        final EditText goalEditText = findViewById(R.id.goalEditText);
        final TextView dateTextView = findViewById(R.id.dateTextView);
        final Button luisterUitleg = findViewById(R.id.vertelBeschrijving);
        ImageView exit = findViewById(R.id.popupExit);
        TextView duurDoelstelling = findViewById(R.id.duurDoelstelling);
        TextView beschrijving = findViewById(R.id.beschrijving);

        final AppDatabase db = Room // Start de lokale database
                .databaseBuilder(getApplicationContext(),AppDatabase.class,"in_shape_db")
                .allowMainThreadQueries() //NIET IN PRODUCTIE!!
                .build();
        final String exercise = getExtras("exercise");
        String interval = getExtras("interval");

        exerciseTextView.setText(exercise);
        duurDoelstelling.setText("1 doelstelling duurt: " + interval + " seconden.");
        beschrijving.setText(getExtras("description"));

        // verlaat de popup als je op het kruisje klikt
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // stop de stem als ze aan het uitleggen is
                try {
                    tts.stop();
                } catch (Exception e) {
                    Log.d(TAG, "exeption", e);
                }
                finish();
            }
        });

        // image of gif van oefening uit een url halen en displaye
        Glide.with(this).load("http://188.166.31.210" + getExtras("video")).diskCacheStrategy(DiskCacheStrategy.ALL).into(exerciseGifImageView);

        luisterUitleg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tts = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
                    @Override
                    public void onInit(int status) {
                        tts.setLanguage(new Locale("nl_NL"));
                        tts.setSpeechRate(0.8f);
                        tts.speak(getExtras("description"), TextToSpeech.QUEUE_FLUSH, null);
                    }
                });
            }
        });


        dateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();

                // haal de date van vandaag op
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                // maar een nieuwe scherm waar een datum gekozen kan worden
                DatePickerDialog dialog = new DatePickerDialog(
                        ExercisePop.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        dateSetListener,
                        year,month,day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });


        dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = month + 1;
                // verander de date in gekozen formaat en zet deze op de pagina
                String date = year + "/" + month + "/" + dayOfMonth;
                dateTextView.setText(date);
            }
        };


        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        // set het scherm op minder dan 100% zodat het een popup is
        getWindow().setLayout((int)(width*.82),(int)(height*0.82));
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // handle alle mogelijke errors af met toast zodat de gebruiker weet wat er moet veranderen
                try {
                    int goal = Integer.valueOf(goalEditText.getText().toString());
                    String date = dateTextView.getText().toString();
                    if (goal > 0) {
                        if (!date.equals("Kies een datum")) {
                            //stop alles in de database
                            ExerciseInfo ei = new ExerciseInfo();
                            ei.setExercise(exercise);
                            ei.setGoal(goal);
                            ei.setProgress(0);
                            ei.setDate(date);
                            db.exerciseInfoDAO().insertAll(ei);
                            try {
                                tts.stop();
                            } catch (Exception e) {
                                Log.d(TAG, "exeption", e);
                            }
                            Toast.makeText(ExercisePop.this, exercise + " toegevoegd " + goalEditText.getText(), Toast.LENGTH_LONG).show();
                            // sluit de activity
                            finish();
                        } else {
                            Toast.makeText(ExercisePop.this, "Geen datum ingevuld!", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(ExercisePop.this, "Vul een tijd boven de 0 in", Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    try {
                        int goal = Integer.valueOf(goalEditText.getText().toString());
                        Toast.makeText(ExercisePop.this, "Deze oefening heeft u al op deze dag gepland!", Toast.LENGTH_LONG).show();
                    } catch (Exception goal) {
                        Toast.makeText(ExercisePop.this, "Geen doelstelling ingevuld!", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

    // haal de meegegeven exercise op
    public String getExtras(String extra){
        Bundle extras = getIntent().getExtras();
        if(extras !=null)
        {
            return(extras.getString(extra));
        }
        return null;
    }
}
